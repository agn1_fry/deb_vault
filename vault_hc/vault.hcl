backend "file" {
        path = "/var/lib/vault"
}

listener "tcp" {
        tls_disable = 0
        tls_cert_file = "/etc/ssl/certs/vault.crt"
        tls_key_file = "/etc/ssl/private/vault.key"

}
