class Vault < FPM::Cookery::Recipe
  homepage 'https://www.vaultproject.io/'
  name     'vault'
  description 'HashiCorp Vault is a tool for securely accessing secrets'

  #config_files '/etc/vault.hcl'

  post_install  'postinst'
  post_uninstall   'postrm'

  def build;end

  def install
    bin('').mkdir
    bin.install ['vault', 'vault']
    %w(run log/vault).each {|p| var(p).mkdir }
    etc('systemd/system/').mkdir
    etc.install workdir('vault.hcl')
    etc('systemd/system/').install workdir('vault.service')
  end
end
